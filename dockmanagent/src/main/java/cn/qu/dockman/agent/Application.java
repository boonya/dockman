package cn.qu.dockman.agent;

import cn.qu.dockman.agent.scheduler.HostScheduler;
import cn.qu.dockman.agent.scheduler.quartz.AutowiringSpringBeanJobFactory;
import cn.qu.dockman.agent.util.DockmanClient;
import cn.qu.dockman.agent.ws.WebSocket;
import org.quartz.Trigger;
import org.quartz.spi.JobFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

import java.io.IOException;
import java.net.URI;

/**
 * Created by zh on 17/3/8.
 */
@SpringBootApplication
@Import({HostScheduler.class})
public class Application {

    @Bean
    public JobFactory jobFactory(ApplicationContext applicationContext) {
        AutowiringSpringBeanJobFactory jobFactory = new AutowiringSpringBeanJobFactory();
        jobFactory.setApplicationContext(applicationContext);
        return jobFactory;
    }

    @Bean
    public SchedulerFactoryBean schedulerFactoryBean(
            JobFactory jobFactory,
            @Qualifier("pingJobTrigger") Trigger pingJobTrigger,
            @Qualifier("hostJobTrigger") Trigger hostJobTrigger,
            @Qualifier("imageListJobTrigger") Trigger imageListJobTrigger,
            @Qualifier("containerListJobTrigger") Trigger containerListJobTrigger,
            @Qualifier("networkJobTrigger") Trigger networkJobTrigger
    ) throws IOException {
        SchedulerFactoryBean factory = new SchedulerFactoryBean();
        //用于quartz集群,QuartzScheduler 启动时更新己存在的Job，这样就不用每次修改targetObject后删除qrtz_job_details表对应记录了
        factory.setOverwriteExistingJobs(true);
        //用于quartz集群,加载quartz数据源
//          factory.setDataSource(dataSource);
        factory.setJobFactory(jobFactory);
        //QuartzScheduler 延时启动，应用启动完20秒后 QuartzScheduler 再启动
        factory.setStartupDelay(10);
        //用于quartz集群,加载quartz数据源配置
//          factory.setQuartzProperties(quartzProperties());
        //注册触发器
        factory.setTriggers(pingJobTrigger, hostJobTrigger, imageListJobTrigger, containerListJobTrigger, networkJobTrigger);

        return factory;
    }

    @Bean
    @ConfigurationProperties(prefix = "docker")
    public DockmanClient.DockerConfig createDockerConfig() {
        return new DockmanClient.DockerConfig();
    }

    @Bean
    public DockmanClient createDockmanClient(DockmanClient.DockerConfig dockerConfig) {
        return new DockmanClient(dockerConfig);
    }

    @Bean
    public ServerEndpointExporter createServerEndpointExporter() {
        return new ServerEndpointExporter();
    }

    @Bean
    public WebSocket createWebSocket(@Value("${token}") String token,DockmanClient dockmanClient,@Value("${server.ws}") URI serverWs) {
        WebSocket webSocket = new WebSocket(serverWs,dockmanClient);
        webSocket.setToken(token);
        return webSocket;
    }




    public static void main(String[] args) {
        new SpringApplication(Application.class).run(args);
    }
}