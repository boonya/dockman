package cn.qu.dockman.agent.scheduler.jobs;

import cn.qu.dockman.agent.scheduler.quartz.WsJob;
import cn.qu.dockman.agent.util.DockmanClient;
import cn.qu.dockman.protocol.command.client.ContainerListCommand;
import com.github.dockerjava.api.model.Container;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static cn.qu.dockman.protocol.command.client.ContainerListCommand.ContainerListBody;

/**
 * Created by zh on 17/3/16.
 */
public class ContainerListJob extends WsJob {

    @Autowired
    private DockmanClient dockmanClient;

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        List<Container> containerList = dockmanClient.listContainersCmd().withShowAll(true).exec();

        List<ContainerListBody> bodyList = new ArrayList<>();
        for (Container container : containerList) {
            ContainerListBody body = new ContainerListBody();
            BeanUtils.copyProperties(container, body);
            bodyList.add(body);
        }

        ContainerListCommand command = ContainerListCommand.create(bodyList.toArray(new ContainerListBody[0]));
        getQueue().send(command);

    }


}
