package cn.qu.dockman.agent.scheduler.jobs;

import cn.qu.dockman.agent.scheduler.quartz.WsJob;
import cn.qu.dockman.agent.util.DockmanClient;
import cn.qu.dockman.protocol.command.client.NetworkListCommand;

import static cn.qu.dockman.protocol.command.client.NetworkListCommand.*;

import com.github.dockerjava.api.model.Image;
import com.github.dockerjava.api.model.Network;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zh on 17/3/16.
 */
public class NetworkJob extends WsJob {

    @Autowired
    private DockmanClient dockmanClient;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        List<Network> networkList = dockmanClient.listNetworksCmd().exec();

        List<NetworkBody> bodyList = new ArrayList<>();
        for (Network network : networkList) {
            NetworkBody body = new NetworkBody();
            BeanUtils.copyProperties(network, body);
            bodyList.add(body);
        }

        NetworkListCommand command = NetworkListCommand.create(bodyList.toArray(new NetworkBody[0]));
        getQueue().send(command);
    }
}
