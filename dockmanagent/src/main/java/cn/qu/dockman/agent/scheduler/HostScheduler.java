package cn.qu.dockman.agent.scheduler;

import cn.qu.dockman.agent.scheduler.jobs.*;
import cn.qu.dockman.agent.util.SendingQueue;
import cn.qu.dockman.agent.ws.WebSocket;
import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

/**
 * Created by zh on 17/3/16.
 */
@Configurable
public class HostScheduler {


    @Autowired
    private WebSocket ws;

    @Bean
    public SendingQueue sendingQueue() {
        SendingQueue queue = new SendingQueue();
        queue.setWebSocket(ws);
        return queue;
    }

    @Bean(name = "hostJob")
    public JobDetailFactoryBean createCpuJob() {
        return createJobDetail(HostJob.class);
    }

    @Bean(name = "hostJobTrigger")
    public CronTriggerFactoryBean createCpuJobTrigger(
            @Qualifier("hostJob") JobDetail jobDetail,
            @Value("${job.host.cron}") String cron,
            @Value("${job.host.delay}") long delay) {
        return createTrigger(jobDetail, cron, delay);
    }

    @Bean(name = "pingJob")
    public JobDetailFactoryBean createPingJob() {
        return createJobDetail(PingJob.class);
    }

    @Bean(name = "pingJobTrigger")
    public CronTriggerFactoryBean createPingJobTrigger(
            @Qualifier("pingJob") JobDetail jobDetail,
            @Value("${job.ping.cron}") String cron,
            @Value("${job.ping.delay}") long delay) {
        return createTrigger(jobDetail, cron, delay);
    }

    @Bean(name = "imageListJob")
    public JobDetailFactoryBean createImageListJob() {
        return createJobDetail(ImageListJob.class);
    }

    @Bean(name = "imageListJobTrigger")
    public CronTriggerFactoryBean createImageListJobTrigger(
            @Qualifier("imageListJob") JobDetail jobDetail,
            @Value("${job.image.cron}") String cron,
            @Value("${job.image.delay}") long delay) {
        return createTrigger(jobDetail, cron, delay);
    }

    @Bean(name = "containerListJob")
    public JobDetailFactoryBean containerListJob() {
        return createJobDetail(ContainerListJob.class);
    }

    @Bean(name = "containerListJobTrigger")
    public CronTriggerFactoryBean containerListJobTrigger(
            @Qualifier("containerListJob") JobDetail jobDetail,
            @Value("${job.container.cron}") String cron,
            @Value("${job.container.delay}") long delay) {
        return createTrigger(jobDetail, cron, delay);
    }

    @Bean(name = "networkJob")
    public JobDetailFactoryBean networkJob() {
        return createJobDetail(NetworkJob.class);
    }

    @Bean(name = "networkJobTrigger")
    public CronTriggerFactoryBean networkJobTrigger(
            @Qualifier("networkJob") JobDetail jobDetail,
            @Value("${job.network.cron}") String cron,
            @Value("${job.network.delay}") long delay) {
        return createTrigger(jobDetail, cron, delay);
    }



    private static JobDetailFactoryBean createJobDetail(Class jobClass) {
        JobDetailFactoryBean factoryBean = new JobDetailFactoryBean();
        factoryBean.setJobClass(jobClass);
        factoryBean.setDurability(true);

        return factoryBean;
    }

    private static CronTriggerFactoryBean createTrigger(
            JobDetail jobDetail, String cron, long delay) {
        CronTriggerFactoryBean factoryBean = new CronTriggerFactoryBean();
        factoryBean.setJobDetail(jobDetail);
        factoryBean.setStartDelay(delay);
        factoryBean.setCronExpression(cron);
        return factoryBean;
    }

}
