package cn.qu.dockman.agent.util;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.*;
import com.github.dockerjava.api.exception.DockerException;
import com.github.dockerjava.api.model.AuthConfig;
import com.github.dockerjava.api.model.Identifier;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by zh on 17/3/8.
 */
public class DockmanClient implements DockerClient, InitializingBean, DisposableBean {

    private DockerConfig dockerConfig;

    private DockerClient dockerClient;

    @Override
    public void afterPropertiesSet() throws Exception {
        DockerClientConfig config = DefaultDockerClientConfig.createDefaultConfigBuilder()
                .withDockerHost(dockerConfig.host)
                .withDockerTlsVerify(dockerConfig.tlsVerify == 1)
//                .withDockerCertPath("/home/user/.docker/certs")
//                .withDockerConfig("/home/user/.docker")
                .withApiVersion(dockerConfig.apiVersion)
//                .withRegistryUrl("https://index.docker.io/v1/")
//                .withRegistryUsername("dockeruser")
//                .withRegistryPassword("ilovedocker")
//                .withRegistryEmail("dockeruser@github.com")
                .build();
        dockerClient = DockerClientBuilder.getInstance(config).build();
    }

    public DockmanClient(DockerConfig dockerConfig) {
        this.dockerConfig = dockerConfig;
    }

    @Override
    public AuthConfig authConfig() throws DockerException {
        return dockerClient.authConfig();
    }

    @Override
    public AuthCmd authCmd() {
        return dockerClient.authCmd();
    }

    @Override
    public InfoCmd infoCmd() {
        return dockerClient.infoCmd();
    }

    @Override
    public PingCmd pingCmd() {
        return dockerClient.pingCmd();
    }

    @Override
    public VersionCmd versionCmd() {
        return dockerClient.versionCmd();
    }

    @Override
    public PullImageCmd pullImageCmd(String repository) {
        return dockerClient.pullImageCmd(repository);
    }

    @Override
    public PushImageCmd pushImageCmd(String name) {
        return dockerClient.pushImageCmd(name);
    }

    @Override
    public PushImageCmd pushImageCmd(Identifier identifier) {
        return dockerClient.pushImageCmd(identifier);
    }

    @Override
    public CreateImageCmd createImageCmd(String repository, InputStream imageStream) {
        return dockerClient.createImageCmd(repository, imageStream);
    }

    @Override
    public LoadImageCmd loadImageCmd(InputStream imageStream) {
        return dockerClient.loadImageCmd(imageStream);
    }

    @Override
    public SearchImagesCmd searchImagesCmd(String term) {
        return dockerClient.searchImagesCmd(term);
    }

    @Override
    public RemoveImageCmd removeImageCmd(String imageId) {
        return dockerClient.removeImageCmd(imageId);
    }

    @Override
    public ListImagesCmd listImagesCmd() {
        return dockerClient.listImagesCmd();
    }

    @Override
    public InspectImageCmd inspectImageCmd(String imageId) {
        return dockerClient.inspectImageCmd(imageId);
    }

    @Override
    public SaveImageCmd saveImageCmd(String name) {
        return dockerClient.saveImageCmd(name);
    }

    @Override
    public ListContainersCmd listContainersCmd() {
        return dockerClient.listContainersCmd();
    }

    @Override
    public CreateContainerCmd createContainerCmd(String image) {
        return dockerClient.createContainerCmd(image);
    }

    @Override
    public StartContainerCmd startContainerCmd(String containerId) {
        return dockerClient.startContainerCmd(containerId);
    }

    @Override
    public ExecCreateCmd execCreateCmd(String containerId) {
        return dockerClient.execCreateCmd(containerId);
    }

    @Override
    public InspectContainerCmd inspectContainerCmd(String containerId) {
        return dockerClient.inspectContainerCmd(containerId);
    }

    @Override
    public RemoveContainerCmd removeContainerCmd(String containerId) {
        return dockerClient.removeContainerCmd(containerId);
    }

    @Override
    public WaitContainerCmd waitContainerCmd(String containerId) {
        return dockerClient.waitContainerCmd(containerId);
    }

    @Override
    public AttachContainerCmd attachContainerCmd(String containerId) {
        return dockerClient.attachContainerCmd(containerId);
    }

    @Override
    public ExecStartCmd execStartCmd(String execId) {
        return dockerClient.execStartCmd(execId);
    }

    @Override
    public InspectExecCmd inspectExecCmd(String execId) {
        return dockerClient.inspectExecCmd(execId);
    }

    @Override
    public LogContainerCmd logContainerCmd(String containerId) {
        return dockerClient.logContainerCmd(containerId);
    }

    @Override
    public CopyArchiveFromContainerCmd copyArchiveFromContainerCmd(String containerId, String resource) {
        return dockerClient.copyArchiveFromContainerCmd(containerId, resource);
    }

    @Override
    public CopyFileFromContainerCmd copyFileFromContainerCmd(String containerId, String resource) {
        return dockerClient.copyFileFromContainerCmd(containerId, resource);
    }

    @Override
    public CopyArchiveToContainerCmd copyArchiveToContainerCmd(String containerId) {
        return dockerClient.copyArchiveToContainerCmd(containerId);
    }

    @Override
    public ContainerDiffCmd containerDiffCmd(String containerId) {
        return dockerClient.containerDiffCmd(containerId);
    }

    @Override
    public StopContainerCmd stopContainerCmd(String containerId) {
        return dockerClient.stopContainerCmd(containerId);
    }

    @Override
    public KillContainerCmd killContainerCmd(String containerId) {
        return dockerClient.killContainerCmd(containerId);
    }

    @Override
    public UpdateContainerCmd updateContainerCmd(String containerId) {
        return dockerClient.updateContainerCmd(containerId);
    }

    @Override
    public RenameContainerCmd renameContainerCmd(String containerId) {
        return dockerClient.renameContainerCmd(containerId);
    }

    @Override
    public RestartContainerCmd restartContainerCmd(String containerId) {
        return dockerClient.restartContainerCmd(containerId);
    }

    @Override
    public CommitCmd commitCmd(String containerId) {
        return dockerClient.commitCmd(containerId);
    }

    @Override
    public BuildImageCmd buildImageCmd() {
        return dockerClient.buildImageCmd();
    }

    @Override
    public BuildImageCmd buildImageCmd(File dockerFileOrFolder) {
        return dockerClient.buildImageCmd(dockerFileOrFolder);
    }

    @Override
    public BuildImageCmd buildImageCmd(InputStream tarInputStream) {
        return dockerClient.buildImageCmd(tarInputStream);
    }

    @Override
    public TopContainerCmd topContainerCmd(String containerId) {
        return dockerClient.topContainerCmd(containerId);
    }

    @Override
    public TagImageCmd tagImageCmd(String imageId, String repository, String tag) {
        return dockerClient.tagImageCmd(imageId, repository, tag);
    }

    @Override
    public PauseContainerCmd pauseContainerCmd(String containerId) {
        return dockerClient.pauseContainerCmd(containerId);
    }

    @Override
    public UnpauseContainerCmd unpauseContainerCmd(String containerId) {
        return dockerClient.unpauseContainerCmd(containerId);
    }

    @Override
    public EventsCmd eventsCmd() {
        return dockerClient.eventsCmd();
    }

    @Override
    public StatsCmd statsCmd(String containerId) {
        return dockerClient.statsCmd(containerId);
    }

    @Override
    public CreateVolumeCmd createVolumeCmd() {
        return dockerClient.createVolumeCmd();
    }

    @Override
    public InspectVolumeCmd inspectVolumeCmd(String name) {
        return dockerClient.inspectVolumeCmd(name);
    }

    @Override
    public RemoveVolumeCmd removeVolumeCmd(String name) {
        return dockerClient.removeVolumeCmd(name);
    }

    @Override
    public ListVolumesCmd listVolumesCmd() {
        return dockerClient.listVolumesCmd();
    }

    @Override
    public ListNetworksCmd listNetworksCmd() {
        return dockerClient.listNetworksCmd();
    }

    @Override
    public InspectNetworkCmd inspectNetworkCmd() {
        return dockerClient.inspectNetworkCmd();
    }

    @Override
    public CreateNetworkCmd createNetworkCmd() {
        return dockerClient.createNetworkCmd();
    }

    @Override
    public RemoveNetworkCmd removeNetworkCmd(String networkId) {
        return dockerClient.removeNetworkCmd(networkId);
    }

    @Override
    public ConnectToNetworkCmd connectToNetworkCmd() {
        return dockerClient.connectToNetworkCmd();
    }

    @Override
    public DisconnectFromNetworkCmd disconnectFromNetworkCmd() {
        return dockerClient.disconnectFromNetworkCmd();
    }

    @Override
    public void close() throws IOException {
        dockerClient.close();
    }

    @Override
    public void destroy() throws Exception {
        close();
    }

    public static class DockerConfig {
        private String host;
        private String apiVersion;
        private String certPath;
        private String config;
        private int tlsVerify;
        private String registryUrl;
        private String registryUsername;
        private String registryPassword;
        private String registryEmail;

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public String getApiVersion() {
            return apiVersion;
        }

        public void setApiVersion(String apiVersion) {
            this.apiVersion = apiVersion;
        }

        public String getCertPath() {
            return certPath;
        }

        public void setCertPath(String certPath) {
            this.certPath = certPath;
        }

        public String getConfig() {
            return config;
        }

        public void setConfig(String config) {
            this.config = config;
        }

        public int getTlsVerify() {
            return tlsVerify;
        }

        public void setTlsVerify(int tlsVerify) {
            this.tlsVerify = tlsVerify;
        }

        public String getRegistryUrl() {
            return registryUrl;
        }

        public void setRegistryUrl(String registryUrl) {
            this.registryUrl = registryUrl;
        }

        public String getRegistryUsername() {
            return registryUsername;
        }

        public void setRegistryUsername(String registryUsername) {
            this.registryUsername = registryUsername;
        }

        public String getRegistryPassword() {
            return registryPassword;
        }

        public void setRegistryPassword(String registryPassword) {
            this.registryPassword = registryPassword;
        }

        public String getRegistryEmail() {
            return registryEmail;
        }

        public void setRegistryEmail(String registryEmail) {
            this.registryEmail = registryEmail;
        }
    }

}
