package cn.qu.dockman.agent.util;

import org.hyperic.sigar.Sigar;

import java.io.File;
import java.nio.file.Paths;

/**
 * Created by zh on 17/3/10.
 */
public class SigarUtils {
    public final static Sigar sigar = initSigar();

    private static Sigar initSigar() {
        try {
            //此处只为得到依赖库文件的目录，可根据实际项目自定义
            String file = Paths.get("config/so").toString();
            File classPath = new File(file);

            String path = System.getProperty("java.library.path");
            String sigarLibPath = classPath.getCanonicalPath();
            if (!path.contains(sigarLibPath)) {
                if (isOSWin()) {
                    path += ";" + sigarLibPath;
                } else {
                    path += ":" + sigarLibPath;
                }
                System.setProperty("java.library.path", path);
            }
            return new Sigar();
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean isOSWin() {
        String OS = System.getProperty("os.name").toLowerCase();
        if (OS.indexOf("win") >= 0) {
            return true;
        } else return false;
    }
}
