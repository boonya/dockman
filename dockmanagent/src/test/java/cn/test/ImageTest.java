package cn.test;

import cn.qu.dockman.agent.util.DockmanClient;
import com.github.dockerjava.api.model.Image;
import com.github.dockerjava.api.model.Info;
import org.junit.Test;

import java.util.List;

/**
 * Created by zh on 17/3/15.
 */
public class ImageTest {

    @Test
    public void testImagesList() throws Exception {
        DockmanClient.DockerConfig config = new DockmanClient.DockerConfig();

//        host: unix:///var/run/docker.sock
//        api_version: 1.23
//        tls_verify: 0
//        cert_path: /home/user/.docker/certs
//        config: /home/user/.docker
//
        config.setHost("unix:///var/run/docker.sock");
        config.setApiVersion("1.23");
        config.setTlsVerify(0);

        DockmanClient client = new DockmanClient(config);

        client.afterPropertiesSet();

        List<Image> images = client.listImagesCmd().exec();
        System.out.println(images);
        Info info = client.infoCmd().exec();
        System.out.println(info);
    }
}
