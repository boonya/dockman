package cn.qu.dockman.protocol.def;

/**
 * Created by zh on 17/3/8.
 */
public class Header {
    private String name;

    public Header(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
