package cn.qu.dockman.protocol.command.server;

import cn.qu.dockman.protocol.command.Command;
import cn.qu.dockman.protocol.def.Body;
import cn.qu.dockman.protocol.def.Header;
import com.google.gson.Gson;

/**
 * Created by zh on 17/3/17.
 */
public class ContainerServiceCommand extends Command<ContainerServiceCommand.ContainerServiceBody> {

    public static final String NAME = "containerService";

    public static ContainerServiceCommand create(ContainerServiceBody[] body) {
        return new ContainerServiceCommand(new Header(NAME), body);
    }

    private ContainerServiceCommand(Header header, ContainerServiceBody[] body) {
        super(header, body);
    }


    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public static class ContainerServiceBody implements Body {
        private Type type;
        private String containerId;

        public String getContainerId() {
            return containerId;
        }

        public void setContainerId(String containerId) {
            this.containerId = containerId;
        }

        public Type getType() {
            return type;
        }

        public void setType(Type type) {
            this.type = type;
        }
    }

    public static enum Type {
        START, STOP
    }
}
