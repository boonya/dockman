package cn.qu.dockman.protocol.command.client;

import cn.qu.dockman.protocol.command.Command;
import cn.qu.dockman.protocol.def.Body;
import cn.qu.dockman.protocol.def.Header;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.github.dockerjava.api.command.ListContainersCmd;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.ContainerPort;
import com.github.dockerjava.core.RemoteApiVersion;
import com.google.gson.Gson;

import java.util.Map;

/**
 * Created by zh on 17/3/15.
 */
public class ContainerListCommand extends Command<ContainerListCommand.ContainerListBody> {

    public static final String NAME = "containerList";

    public static ContainerListCommand create(ContainerListBody[] body) {
        return new ContainerListCommand(new Header(NAME), body);
    }

    private ContainerListCommand(Header header, ContainerListCommand.ContainerListBody[] body) {
        super(header, body);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public static class ContainerListBody implements Body {

//        private String command;

        private Long created;

        private String id;

//        private String image;

        private String imageId;

        private String[] names;

//        public ContainerPort[] ports;

        public Map<String, String> labels;

        private String status;

//        private Long sizeRw;

//        private Long sizeRootFs;


        public Long getCreated() {
            return created;
        }

        public void setCreated(Long created) {
            this.created = created;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String[] getNames() {
            return names;
        }

        public void setNames(String[] names) {
            this.names = names;
        }

        public Map<String, String> getLabels() {
            return labels;
        }

        public void setLabels(Map<String, String> labels) {
            this.labels = labels;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getImageId() {
            return imageId;
        }

        public void setImageId(String imageId) {
            this.imageId = imageId;
        }
    }
}