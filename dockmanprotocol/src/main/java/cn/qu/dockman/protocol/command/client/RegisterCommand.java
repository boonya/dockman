package cn.qu.dockman.protocol.command.client;

import cn.qu.dockman.protocol.command.Command;
import cn.qu.dockman.protocol.def.Body;
import cn.qu.dockman.protocol.def.Header;
import com.google.gson.Gson;

/**
 * Created by zh on 17/3/8.
 */
public class RegisterCommand extends Command<RegisterCommand.RegisterBody> {

    public static final String NAME = "register";

    public static RegisterCommand create(String token) {
        RegisterBody body = new RegisterCommand.RegisterBody();
        body.setToken(token);
        return new RegisterCommand(new Header(NAME), new RegisterBody[]{body});
    }

    private RegisterCommand(Header header, RegisterBody[] body) {
        super(header, body);
    }

    @Override
    public String toString() {

        return new Gson().toJson(this);
    }

    public static class RegisterBody implements Body {
        private String token;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
    }

}
