package cn.qu.dockman.protocol.command.client;

import cn.qu.dockman.protocol.command.Command;
import cn.qu.dockman.protocol.def.Body;
import cn.qu.dockman.protocol.def.Header;
import com.google.gson.Gson;

/**
 * Created by zh on 17/3/15.
 */
public class NetworkListCommand extends Command<NetworkListCommand.NetworkBody> {

    public static final String NAME = "networkList";

    public static NetworkListCommand create(NetworkBody[] body) {
        return new NetworkListCommand(new Header(NAME), body);
    }

    private NetworkListCommand(Header header, NetworkListCommand.NetworkBody[] body) {
        super(header, body);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public static class NetworkBody implements Body {

        private String id;
        private String name;
        private String scope;
        private String driver;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getScope() {
            return scope;
        }

        public void setScope(String scope) {
            this.scope = scope;
        }

        public String getDriver() {
            return driver;
        }

        public void setDriver(String driver) {
            this.driver = driver;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}