package cn.qu.dockman.protocol.command;

import cn.qu.dockman.protocol.def.Body;
import cn.qu.dockman.protocol.def.Header;

/**
 * Created by zh on 17/3/8.
 */
public class ErrorCommand extends Command {
    protected ErrorCommand(Header header, Body[] body) {
        super(header, body);
    }
}
