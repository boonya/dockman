package cn.qu.dockman.protocol.command.client;

import cn.qu.dockman.protocol.command.Command;
import cn.qu.dockman.protocol.def.Body;
import cn.qu.dockman.protocol.def.Header;
import com.google.gson.Gson;


/**
 * Created by zh on 17/3/9.
 */
public class MetricsCommand extends Command<MetricsCommand.MetricsBody> {

    public static final String NAME = "metrics";

    public static MetricsCommand create(String name, String value) {
        MetricsBody body = new MetricsBody();
        body.setName(name);
        body.setValue(value);
        return new MetricsCommand(new Header(NAME), new MetricsBody[]{body});
    }

    public static MetricsCommand create(MetricsBody[] bodys) {
        return new MetricsCommand(new Header(NAME), bodys);
    }

    private MetricsCommand(Header header, MetricsCommand.MetricsBody[] body) {
        super(header, body);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public static class MetricsBody implements Body {
        private String value;
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
