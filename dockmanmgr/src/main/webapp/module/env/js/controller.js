/**
 * Created by zh on 17/3/7.
 */

angular.module('dockman.env.controller', ['dockman.env.service'])
    .controller('envTopController', ['$scope', 'EnvResource', function ($scope, EnvResource) {

        $scope.items = [];
        $scope.findAll = function () {
            $scope.items = EnvResource.query()
        };

        $scope.findAll();

    }]);