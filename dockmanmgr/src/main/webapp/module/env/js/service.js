/**
 * Created by zh on 17/3/7.
 */
angular.module('dockman.env.service', ['ngResource'])
    .factory('EnvResource', ['$resource', function ($resource) {
        return $resource('/env/:id', {}, {
            'put': {
                method: 'PUT'
            }
        })
    }])