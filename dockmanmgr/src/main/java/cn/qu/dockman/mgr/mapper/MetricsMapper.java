package cn.qu.dockman.mgr.mapper;

import cn.qu.dockman.mgr.entity.Metrics;
import cn.qu.dockman.mgr.example.MetricsExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface MetricsMapper {
    long countByExample(MetricsExample example);

    int deleteByExample(MetricsExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Metrics record);

    int insertSelective(Metrics record);

    List<Metrics> selectByExample(MetricsExample example);

    Metrics selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Metrics record, @Param("example") MetricsExample example);

    int updateByExample(@Param("record") Metrics record, @Param("example") MetricsExample example);

    int updateByPrimaryKeySelective(Metrics record);

    int updateByPrimaryKey(Metrics record);
}