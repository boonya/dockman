package cn.qu.dockman.mgr.example;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MetricsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public MetricsExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andMetricsNameIsNull() {
            addCriterion("metrics_name is null");
            return (Criteria) this;
        }

        public Criteria andMetricsNameIsNotNull() {
            addCriterion("metrics_name is not null");
            return (Criteria) this;
        }

        public Criteria andMetricsNameEqualTo(String value) {
            addCriterion("metrics_name =", value, "metricsName");
            return (Criteria) this;
        }

        public Criteria andMetricsNameNotEqualTo(String value) {
            addCriterion("metrics_name <>", value, "metricsName");
            return (Criteria) this;
        }

        public Criteria andMetricsNameGreaterThan(String value) {
            addCriterion("metrics_name >", value, "metricsName");
            return (Criteria) this;
        }

        public Criteria andMetricsNameGreaterThanOrEqualTo(String value) {
            addCriterion("metrics_name >=", value, "metricsName");
            return (Criteria) this;
        }

        public Criteria andMetricsNameLessThan(String value) {
            addCriterion("metrics_name <", value, "metricsName");
            return (Criteria) this;
        }

        public Criteria andMetricsNameLessThanOrEqualTo(String value) {
            addCriterion("metrics_name <=", value, "metricsName");
            return (Criteria) this;
        }

        public Criteria andMetricsNameLike(String value) {
            addCriterion("metrics_name like", value, "metricsName");
            return (Criteria) this;
        }

        public Criteria andMetricsNameNotLike(String value) {
            addCriterion("metrics_name not like", value, "metricsName");
            return (Criteria) this;
        }

        public Criteria andMetricsNameIn(List<String> values) {
            addCriterion("metrics_name in", values, "metricsName");
            return (Criteria) this;
        }

        public Criteria andMetricsNameNotIn(List<String> values) {
            addCriterion("metrics_name not in", values, "metricsName");
            return (Criteria) this;
        }

        public Criteria andMetricsNameBetween(String value1, String value2) {
            addCriterion("metrics_name between", value1, value2, "metricsName");
            return (Criteria) this;
        }

        public Criteria andMetricsNameNotBetween(String value1, String value2) {
            addCriterion("metrics_name not between", value1, value2, "metricsName");
            return (Criteria) this;
        }

        public Criteria andMetricsValueIsNull() {
            addCriterion("metrics_value is null");
            return (Criteria) this;
        }

        public Criteria andMetricsValueIsNotNull() {
            addCriterion("metrics_value is not null");
            return (Criteria) this;
        }

        public Criteria andMetricsValueEqualTo(String value) {
            addCriterion("metrics_value =", value, "metricsValue");
            return (Criteria) this;
        }

        public Criteria andMetricsValueNotEqualTo(String value) {
            addCriterion("metrics_value <>", value, "metricsValue");
            return (Criteria) this;
        }

        public Criteria andMetricsValueGreaterThan(String value) {
            addCriterion("metrics_value >", value, "metricsValue");
            return (Criteria) this;
        }

        public Criteria andMetricsValueGreaterThanOrEqualTo(String value) {
            addCriterion("metrics_value >=", value, "metricsValue");
            return (Criteria) this;
        }

        public Criteria andMetricsValueLessThan(String value) {
            addCriterion("metrics_value <", value, "metricsValue");
            return (Criteria) this;
        }

        public Criteria andMetricsValueLessThanOrEqualTo(String value) {
            addCriterion("metrics_value <=", value, "metricsValue");
            return (Criteria) this;
        }

        public Criteria andMetricsValueLike(String value) {
            addCriterion("metrics_value like", value, "metricsValue");
            return (Criteria) this;
        }

        public Criteria andMetricsValueNotLike(String value) {
            addCriterion("metrics_value not like", value, "metricsValue");
            return (Criteria) this;
        }

        public Criteria andMetricsValueIn(List<String> values) {
            addCriterion("metrics_value in", values, "metricsValue");
            return (Criteria) this;
        }

        public Criteria andMetricsValueNotIn(List<String> values) {
            addCriterion("metrics_value not in", values, "metricsValue");
            return (Criteria) this;
        }

        public Criteria andMetricsValueBetween(String value1, String value2) {
            addCriterion("metrics_value between", value1, value2, "metricsValue");
            return (Criteria) this;
        }

        public Criteria andMetricsValueNotBetween(String value1, String value2) {
            addCriterion("metrics_value not between", value1, value2, "metricsValue");
            return (Criteria) this;
        }

        public Criteria andMetricsTimeIsNull() {
            addCriterion("metrics_time is null");
            return (Criteria) this;
        }

        public Criteria andMetricsTimeIsNotNull() {
            addCriterion("metrics_time is not null");
            return (Criteria) this;
        }

        public Criteria andMetricsTimeEqualTo(Date value) {
            addCriterion("metrics_time =", value, "metricsTime");
            return (Criteria) this;
        }

        public Criteria andMetricsTimeNotEqualTo(Date value) {
            addCriterion("metrics_time <>", value, "metricsTime");
            return (Criteria) this;
        }

        public Criteria andMetricsTimeGreaterThan(Date value) {
            addCriterion("metrics_time >", value, "metricsTime");
            return (Criteria) this;
        }

        public Criteria andMetricsTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("metrics_time >=", value, "metricsTime");
            return (Criteria) this;
        }

        public Criteria andMetricsTimeLessThan(Date value) {
            addCriterion("metrics_time <", value, "metricsTime");
            return (Criteria) this;
        }

        public Criteria andMetricsTimeLessThanOrEqualTo(Date value) {
            addCriterion("metrics_time <=", value, "metricsTime");
            return (Criteria) this;
        }

        public Criteria andMetricsTimeIn(List<Date> values) {
            addCriterion("metrics_time in", values, "metricsTime");
            return (Criteria) this;
        }

        public Criteria andMetricsTimeNotIn(List<Date> values) {
            addCriterion("metrics_time not in", values, "metricsTime");
            return (Criteria) this;
        }

        public Criteria andMetricsTimeBetween(Date value1, Date value2) {
            addCriterion("metrics_time between", value1, value2, "metricsTime");
            return (Criteria) this;
        }

        public Criteria andMetricsTimeNotBetween(Date value1, Date value2) {
            addCriterion("metrics_time not between", value1, value2, "metricsTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}