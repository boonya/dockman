package cn.qu.dockman.mgr.service;

import cn.qu.dockman.mgr.entity.Container;
import cn.qu.dockman.mgr.entity.Image;

import java.util.List;

/**
 * Created by zh on 17/3/10.
 */
public interface ContainerService {

    Container findContainer(int hostId, int imageId, String containerId);

    void saveOrUpdate(Container container);

    List<Container> findByHostId(int hostId);

    void delete(Container container);
}
