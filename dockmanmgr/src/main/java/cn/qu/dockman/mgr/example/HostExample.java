package cn.qu.dockman.mgr.example;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HostExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public HostExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andHostNameIsNull() {
            addCriterion("host_name is null");
            return (Criteria) this;
        }

        public Criteria andHostNameIsNotNull() {
            addCriterion("host_name is not null");
            return (Criteria) this;
        }

        public Criteria andHostNameEqualTo(String value) {
            addCriterion("host_name =", value, "hostName");
            return (Criteria) this;
        }

        public Criteria andHostNameNotEqualTo(String value) {
            addCriterion("host_name <>", value, "hostName");
            return (Criteria) this;
        }

        public Criteria andHostNameGreaterThan(String value) {
            addCriterion("host_name >", value, "hostName");
            return (Criteria) this;
        }

        public Criteria andHostNameGreaterThanOrEqualTo(String value) {
            addCriterion("host_name >=", value, "hostName");
            return (Criteria) this;
        }

        public Criteria andHostNameLessThan(String value) {
            addCriterion("host_name <", value, "hostName");
            return (Criteria) this;
        }

        public Criteria andHostNameLessThanOrEqualTo(String value) {
            addCriterion("host_name <=", value, "hostName");
            return (Criteria) this;
        }

        public Criteria andHostNameLike(String value) {
            addCriterion("host_name like", value, "hostName");
            return (Criteria) this;
        }

        public Criteria andHostNameNotLike(String value) {
            addCriterion("host_name not like", value, "hostName");
            return (Criteria) this;
        }

        public Criteria andHostNameIn(List<String> values) {
            addCriterion("host_name in", values, "hostName");
            return (Criteria) this;
        }

        public Criteria andHostNameNotIn(List<String> values) {
            addCriterion("host_name not in", values, "hostName");
            return (Criteria) this;
        }

        public Criteria andHostNameBetween(String value1, String value2) {
            addCriterion("host_name between", value1, value2, "hostName");
            return (Criteria) this;
        }

        public Criteria andHostNameNotBetween(String value1, String value2) {
            addCriterion("host_name not between", value1, value2, "hostName");
            return (Criteria) this;
        }

        public Criteria andHostIpIsNull() {
            addCriterion("host_ip is null");
            return (Criteria) this;
        }

        public Criteria andHostIpIsNotNull() {
            addCriterion("host_ip is not null");
            return (Criteria) this;
        }

        public Criteria andHostIpEqualTo(String value) {
            addCriterion("host_ip =", value, "hostIp");
            return (Criteria) this;
        }

        public Criteria andHostIpNotEqualTo(String value) {
            addCriterion("host_ip <>", value, "hostIp");
            return (Criteria) this;
        }

        public Criteria andHostIpGreaterThan(String value) {
            addCriterion("host_ip >", value, "hostIp");
            return (Criteria) this;
        }

        public Criteria andHostIpGreaterThanOrEqualTo(String value) {
            addCriterion("host_ip >=", value, "hostIp");
            return (Criteria) this;
        }

        public Criteria andHostIpLessThan(String value) {
            addCriterion("host_ip <", value, "hostIp");
            return (Criteria) this;
        }

        public Criteria andHostIpLessThanOrEqualTo(String value) {
            addCriterion("host_ip <=", value, "hostIp");
            return (Criteria) this;
        }

        public Criteria andHostIpLike(String value) {
            addCriterion("host_ip like", value, "hostIp");
            return (Criteria) this;
        }

        public Criteria andHostIpNotLike(String value) {
            addCriterion("host_ip not like", value, "hostIp");
            return (Criteria) this;
        }

        public Criteria andHostIpIn(List<String> values) {
            addCriterion("host_ip in", values, "hostIp");
            return (Criteria) this;
        }

        public Criteria andHostIpNotIn(List<String> values) {
            addCriterion("host_ip not in", values, "hostIp");
            return (Criteria) this;
        }

        public Criteria andHostIpBetween(String value1, String value2) {
            addCriterion("host_ip between", value1, value2, "hostIp");
            return (Criteria) this;
        }

        public Criteria andHostIpNotBetween(String value1, String value2) {
            addCriterion("host_ip not between", value1, value2, "hostIp");
            return (Criteria) this;
        }

        public Criteria andHostCpuIsNull() {
            addCriterion("host_cpu is null");
            return (Criteria) this;
        }

        public Criteria andHostCpuIsNotNull() {
            addCriterion("host_cpu is not null");
            return (Criteria) this;
        }

        public Criteria andHostCpuEqualTo(Integer value) {
            addCriterion("host_cpu =", value, "hostCpu");
            return (Criteria) this;
        }

        public Criteria andHostCpuNotEqualTo(Integer value) {
            addCriterion("host_cpu <>", value, "hostCpu");
            return (Criteria) this;
        }

        public Criteria andHostCpuGreaterThan(Integer value) {
            addCriterion("host_cpu >", value, "hostCpu");
            return (Criteria) this;
        }

        public Criteria andHostCpuGreaterThanOrEqualTo(Integer value) {
            addCriterion("host_cpu >=", value, "hostCpu");
            return (Criteria) this;
        }

        public Criteria andHostCpuLessThan(Integer value) {
            addCriterion("host_cpu <", value, "hostCpu");
            return (Criteria) this;
        }

        public Criteria andHostCpuLessThanOrEqualTo(Integer value) {
            addCriterion("host_cpu <=", value, "hostCpu");
            return (Criteria) this;
        }

        public Criteria andHostCpuIn(List<Integer> values) {
            addCriterion("host_cpu in", values, "hostCpu");
            return (Criteria) this;
        }

        public Criteria andHostCpuNotIn(List<Integer> values) {
            addCriterion("host_cpu not in", values, "hostCpu");
            return (Criteria) this;
        }

        public Criteria andHostCpuBetween(Integer value1, Integer value2) {
            addCriterion("host_cpu between", value1, value2, "hostCpu");
            return (Criteria) this;
        }

        public Criteria andHostCpuNotBetween(Integer value1, Integer value2) {
            addCriterion("host_cpu not between", value1, value2, "hostCpu");
            return (Criteria) this;
        }

        public Criteria andHostCpuUsedIsNull() {
            addCriterion("host_cpu_used is null");
            return (Criteria) this;
        }

        public Criteria andHostCpuUsedIsNotNull() {
            addCriterion("host_cpu_used is not null");
            return (Criteria) this;
        }

        public Criteria andHostCpuUsedEqualTo(Integer value) {
            addCriterion("host_cpu_used =", value, "hostCpuUsed");
            return (Criteria) this;
        }

        public Criteria andHostCpuUsedNotEqualTo(Integer value) {
            addCriterion("host_cpu_used <>", value, "hostCpuUsed");
            return (Criteria) this;
        }

        public Criteria andHostCpuUsedGreaterThan(Integer value) {
            addCriterion("host_cpu_used >", value, "hostCpuUsed");
            return (Criteria) this;
        }

        public Criteria andHostCpuUsedGreaterThanOrEqualTo(Integer value) {
            addCriterion("host_cpu_used >=", value, "hostCpuUsed");
            return (Criteria) this;
        }

        public Criteria andHostCpuUsedLessThan(Integer value) {
            addCriterion("host_cpu_used <", value, "hostCpuUsed");
            return (Criteria) this;
        }

        public Criteria andHostCpuUsedLessThanOrEqualTo(Integer value) {
            addCriterion("host_cpu_used <=", value, "hostCpuUsed");
            return (Criteria) this;
        }

        public Criteria andHostCpuUsedIn(List<Integer> values) {
            addCriterion("host_cpu_used in", values, "hostCpuUsed");
            return (Criteria) this;
        }

        public Criteria andHostCpuUsedNotIn(List<Integer> values) {
            addCriterion("host_cpu_used not in", values, "hostCpuUsed");
            return (Criteria) this;
        }

        public Criteria andHostCpuUsedBetween(Integer value1, Integer value2) {
            addCriterion("host_cpu_used between", value1, value2, "hostCpuUsed");
            return (Criteria) this;
        }

        public Criteria andHostCpuUsedNotBetween(Integer value1, Integer value2) {
            addCriterion("host_cpu_used not between", value1, value2, "hostCpuUsed");
            return (Criteria) this;
        }

        public Criteria andHostMemIsNull() {
            addCriterion("host_mem is null");
            return (Criteria) this;
        }

        public Criteria andHostMemIsNotNull() {
            addCriterion("host_mem is not null");
            return (Criteria) this;
        }

        public Criteria andHostMemEqualTo(Integer value) {
            addCriterion("host_mem =", value, "hostMem");
            return (Criteria) this;
        }

        public Criteria andHostMemNotEqualTo(Integer value) {
            addCriterion("host_mem <>", value, "hostMem");
            return (Criteria) this;
        }

        public Criteria andHostMemGreaterThan(Integer value) {
            addCriterion("host_mem >", value, "hostMem");
            return (Criteria) this;
        }

        public Criteria andHostMemGreaterThanOrEqualTo(Integer value) {
            addCriterion("host_mem >=", value, "hostMem");
            return (Criteria) this;
        }

        public Criteria andHostMemLessThan(Integer value) {
            addCriterion("host_mem <", value, "hostMem");
            return (Criteria) this;
        }

        public Criteria andHostMemLessThanOrEqualTo(Integer value) {
            addCriterion("host_mem <=", value, "hostMem");
            return (Criteria) this;
        }

        public Criteria andHostMemIn(List<Integer> values) {
            addCriterion("host_mem in", values, "hostMem");
            return (Criteria) this;
        }

        public Criteria andHostMemNotIn(List<Integer> values) {
            addCriterion("host_mem not in", values, "hostMem");
            return (Criteria) this;
        }

        public Criteria andHostMemBetween(Integer value1, Integer value2) {
            addCriterion("host_mem between", value1, value2, "hostMem");
            return (Criteria) this;
        }

        public Criteria andHostMemNotBetween(Integer value1, Integer value2) {
            addCriterion("host_mem not between", value1, value2, "hostMem");
            return (Criteria) this;
        }

        public Criteria andHostMemUsedIsNull() {
            addCriterion("host_mem_used is null");
            return (Criteria) this;
        }

        public Criteria andHostMemUsedIsNotNull() {
            addCriterion("host_mem_used is not null");
            return (Criteria) this;
        }

        public Criteria andHostMemUsedEqualTo(Integer value) {
            addCriterion("host_mem_used =", value, "hostMemUsed");
            return (Criteria) this;
        }

        public Criteria andHostMemUsedNotEqualTo(Integer value) {
            addCriterion("host_mem_used <>", value, "hostMemUsed");
            return (Criteria) this;
        }

        public Criteria andHostMemUsedGreaterThan(Integer value) {
            addCriterion("host_mem_used >", value, "hostMemUsed");
            return (Criteria) this;
        }

        public Criteria andHostMemUsedGreaterThanOrEqualTo(Integer value) {
            addCriterion("host_mem_used >=", value, "hostMemUsed");
            return (Criteria) this;
        }

        public Criteria andHostMemUsedLessThan(Integer value) {
            addCriterion("host_mem_used <", value, "hostMemUsed");
            return (Criteria) this;
        }

        public Criteria andHostMemUsedLessThanOrEqualTo(Integer value) {
            addCriterion("host_mem_used <=", value, "hostMemUsed");
            return (Criteria) this;
        }

        public Criteria andHostMemUsedIn(List<Integer> values) {
            addCriterion("host_mem_used in", values, "hostMemUsed");
            return (Criteria) this;
        }

        public Criteria andHostMemUsedNotIn(List<Integer> values) {
            addCriterion("host_mem_used not in", values, "hostMemUsed");
            return (Criteria) this;
        }

        public Criteria andHostMemUsedBetween(Integer value1, Integer value2) {
            addCriterion("host_mem_used between", value1, value2, "hostMemUsed");
            return (Criteria) this;
        }

        public Criteria andHostMemUsedNotBetween(Integer value1, Integer value2) {
            addCriterion("host_mem_used not between", value1, value2, "hostMemUsed");
            return (Criteria) this;
        }

        public Criteria andHostDiskIsNull() {
            addCriterion("host_disk is null");
            return (Criteria) this;
        }

        public Criteria andHostDiskIsNotNull() {
            addCriterion("host_disk is not null");
            return (Criteria) this;
        }

        public Criteria andHostDiskEqualTo(Integer value) {
            addCriterion("host_disk =", value, "hostDisk");
            return (Criteria) this;
        }

        public Criteria andHostDiskNotEqualTo(Integer value) {
            addCriterion("host_disk <>", value, "hostDisk");
            return (Criteria) this;
        }

        public Criteria andHostDiskGreaterThan(Integer value) {
            addCriterion("host_disk >", value, "hostDisk");
            return (Criteria) this;
        }

        public Criteria andHostDiskGreaterThanOrEqualTo(Integer value) {
            addCriterion("host_disk >=", value, "hostDisk");
            return (Criteria) this;
        }

        public Criteria andHostDiskLessThan(Integer value) {
            addCriterion("host_disk <", value, "hostDisk");
            return (Criteria) this;
        }

        public Criteria andHostDiskLessThanOrEqualTo(Integer value) {
            addCriterion("host_disk <=", value, "hostDisk");
            return (Criteria) this;
        }

        public Criteria andHostDiskIn(List<Integer> values) {
            addCriterion("host_disk in", values, "hostDisk");
            return (Criteria) this;
        }

        public Criteria andHostDiskNotIn(List<Integer> values) {
            addCriterion("host_disk not in", values, "hostDisk");
            return (Criteria) this;
        }

        public Criteria andHostDiskBetween(Integer value1, Integer value2) {
            addCriterion("host_disk between", value1, value2, "hostDisk");
            return (Criteria) this;
        }

        public Criteria andHostDiskNotBetween(Integer value1, Integer value2) {
            addCriterion("host_disk not between", value1, value2, "hostDisk");
            return (Criteria) this;
        }

        public Criteria andHostDiskUsedIsNull() {
            addCriterion("host_disk_used is null");
            return (Criteria) this;
        }

        public Criteria andHostDiskUsedIsNotNull() {
            addCriterion("host_disk_used is not null");
            return (Criteria) this;
        }

        public Criteria andHostDiskUsedEqualTo(Integer value) {
            addCriterion("host_disk_used =", value, "hostDiskUsed");
            return (Criteria) this;
        }

        public Criteria andHostDiskUsedNotEqualTo(Integer value) {
            addCriterion("host_disk_used <>", value, "hostDiskUsed");
            return (Criteria) this;
        }

        public Criteria andHostDiskUsedGreaterThan(Integer value) {
            addCriterion("host_disk_used >", value, "hostDiskUsed");
            return (Criteria) this;
        }

        public Criteria andHostDiskUsedGreaterThanOrEqualTo(Integer value) {
            addCriterion("host_disk_used >=", value, "hostDiskUsed");
            return (Criteria) this;
        }

        public Criteria andHostDiskUsedLessThan(Integer value) {
            addCriterion("host_disk_used <", value, "hostDiskUsed");
            return (Criteria) this;
        }

        public Criteria andHostDiskUsedLessThanOrEqualTo(Integer value) {
            addCriterion("host_disk_used <=", value, "hostDiskUsed");
            return (Criteria) this;
        }

        public Criteria andHostDiskUsedIn(List<Integer> values) {
            addCriterion("host_disk_used in", values, "hostDiskUsed");
            return (Criteria) this;
        }

        public Criteria andHostDiskUsedNotIn(List<Integer> values) {
            addCriterion("host_disk_used not in", values, "hostDiskUsed");
            return (Criteria) this;
        }

        public Criteria andHostDiskUsedBetween(Integer value1, Integer value2) {
            addCriterion("host_disk_used between", value1, value2, "hostDiskUsed");
            return (Criteria) this;
        }

        public Criteria andHostDiskUsedNotBetween(Integer value1, Integer value2) {
            addCriterion("host_disk_used not between", value1, value2, "hostDiskUsed");
            return (Criteria) this;
        }

        public Criteria andHostStatusIsNull() {
            addCriterion("host_status is null");
            return (Criteria) this;
        }

        public Criteria andHostStatusIsNotNull() {
            addCriterion("host_status is not null");
            return (Criteria) this;
        }

        public Criteria andHostStatusEqualTo(Integer value) {
            addCriterion("host_status =", value, "hostStatus");
            return (Criteria) this;
        }

        public Criteria andHostStatusNotEqualTo(Integer value) {
            addCriterion("host_status <>", value, "hostStatus");
            return (Criteria) this;
        }

        public Criteria andHostStatusGreaterThan(Integer value) {
            addCriterion("host_status >", value, "hostStatus");
            return (Criteria) this;
        }

        public Criteria andHostStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("host_status >=", value, "hostStatus");
            return (Criteria) this;
        }

        public Criteria andHostStatusLessThan(Integer value) {
            addCriterion("host_status <", value, "hostStatus");
            return (Criteria) this;
        }

        public Criteria andHostStatusLessThanOrEqualTo(Integer value) {
            addCriterion("host_status <=", value, "hostStatus");
            return (Criteria) this;
        }

        public Criteria andHostStatusIn(List<Integer> values) {
            addCriterion("host_status in", values, "hostStatus");
            return (Criteria) this;
        }

        public Criteria andHostStatusNotIn(List<Integer> values) {
            addCriterion("host_status not in", values, "hostStatus");
            return (Criteria) this;
        }

        public Criteria andHostStatusBetween(Integer value1, Integer value2) {
            addCriterion("host_status between", value1, value2, "hostStatus");
            return (Criteria) this;
        }

        public Criteria andHostStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("host_status not between", value1, value2, "hostStatus");
            return (Criteria) this;
        }

        public Criteria andEnvIdIsNull() {
            addCriterion("env_id is null");
            return (Criteria) this;
        }

        public Criteria andEnvIdIsNotNull() {
            addCriterion("env_id is not null");
            return (Criteria) this;
        }

        public Criteria andEnvIdEqualTo(Integer value) {
            addCriterion("env_id =", value, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdNotEqualTo(Integer value) {
            addCriterion("env_id <>", value, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdGreaterThan(Integer value) {
            addCriterion("env_id >", value, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("env_id >=", value, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdLessThan(Integer value) {
            addCriterion("env_id <", value, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdLessThanOrEqualTo(Integer value) {
            addCriterion("env_id <=", value, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdIn(List<Integer> values) {
            addCriterion("env_id in", values, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdNotIn(List<Integer> values) {
            addCriterion("env_id not in", values, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdBetween(Integer value1, Integer value2) {
            addCriterion("env_id between", value1, value2, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdNotBetween(Integer value1, Integer value2) {
            addCriterion("env_id not between", value1, value2, "envId");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}