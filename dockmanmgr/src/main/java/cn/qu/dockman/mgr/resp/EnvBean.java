package cn.qu.dockman.mgr.resp;

import cn.qu.dockman.mgr.entity.Env;

/**
 * Created by zh on 17/3/17.
 */
public class EnvBean {
    private Env env;
    private int hostCount;
    private int containerCount;

    public Env getEnv() {
        return env;
    }

    public void setEnv(Env env) {
        this.env = env;
    }

    public int getHostCount() {
        return hostCount;
    }

    public void setHostCount(int hostCount) {
        this.hostCount = hostCount;
    }

    public int getContainerCount() {
        return containerCount;
    }

    public void setContainerCount(int containerCount) {
        this.containerCount = containerCount;
    }
}
