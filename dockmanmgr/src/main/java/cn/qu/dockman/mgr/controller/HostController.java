package cn.qu.dockman.mgr.controller;

import cn.qu.dockman.mgr.entity.Host;
import cn.qu.dockman.mgr.service.HostService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by zh on 17/3/7.
 */
@RestController
@RequestMapping("/host")
public class HostController {

    @Autowired
    private HostService hostService;

    @RequestMapping(value = "/{envId}", method = RequestMethod.GET)
    public PageInfo<Host> findAll(@PathVariable int envId,
                                  @RequestParam(defaultValue = "1") int pageNo,
                                  @RequestParam(defaultValue = "10") int pageSize) {
        return PageHelper.startPage(pageNo, pageSize).doSelectPageInfo(() -> hostService.findByEnvId(envId));
    }

    @RequestMapping(value = "/info/{id}", method = RequestMethod.GET)
    public Host findHost(@PathVariable int id) {
        return hostService.findById(id);
    }

}
