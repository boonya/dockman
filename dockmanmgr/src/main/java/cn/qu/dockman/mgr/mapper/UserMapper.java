package cn.qu.dockman.mgr.mapper;

import cn.qu.dockman.mgr.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Created by zh on 2017/3/5.
 */
@Mapper
public interface UserMapper {

    int save(User user);

    User selectById(Integer id);

    int updateById(User user);

    int deleteById(Integer id);

    List<User> queryAll();

}
