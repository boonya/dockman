package cn.qu.dockman.mgr.ws.exec;

import cn.qu.dockman.mgr.disparte.ArgDef;
import cn.qu.dockman.mgr.disparte.DispartMgr;
import cn.qu.dockman.protocol.command.Command;
import cn.qu.dockman.protocol.command.client.RegisterCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by zh on 17/3/16.
 */
public class RegisterCommandParser extends CommandParser {
    private static final Logger logger = LoggerFactory.getLogger(RegisterCommandParser.class);

    private DispartMgr dispartMgr;

    @Override
    public void before() {
        this.dispartMgr = getApplicationContext().getBean(DispartMgr.class);
    }

    @Override
    public void parse(Command command) {
        RegisterCommand cmd = (RegisterCommand) command;
        ArgDef def = new ArgDef(cmd.getHeader().getName(), this.getHost(), this.getPort(), this.getHostname());
        RegisterCommand.RegisterBody[] bodys = cmd.getBody();
        if (bodys == null || bodys.length != 1) {
            logger.error("接收到无效的主机注册请求，来源：{}:{}", this.getHost(), this.getPort());
        } else {
            def.setData(bodys[0].getToken());
            dispartMgr.notifyObservers(def);
        }
    }
}
