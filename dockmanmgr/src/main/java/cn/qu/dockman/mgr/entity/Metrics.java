package cn.qu.dockman.mgr.entity;

import java.util.Date;

public class Metrics {
    private Integer id;

    private String metricsName;

    private String metricsValue;

    private Date metricsTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMetricsName() {
        return metricsName;
    }

    public void setMetricsName(String metricsName) {
        this.metricsName = metricsName == null ? null : metricsName.trim();
    }

    public String getMetricsValue() {
        return metricsValue;
    }

    public void setMetricsValue(String metricsValue) {
        this.metricsValue = metricsValue == null ? null : metricsValue.trim();
    }

    public Date getMetricsTime() {
        return metricsTime;
    }

    public void setMetricsTime(Date metricsTime) {
        this.metricsTime = metricsTime;
    }
}