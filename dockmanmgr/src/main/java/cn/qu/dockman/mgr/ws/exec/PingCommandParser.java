package cn.qu.dockman.mgr.ws.exec;

import cn.qu.dockman.mgr.disparte.ArgDef;
import cn.qu.dockman.mgr.disparte.DispartMgr;
import cn.qu.dockman.protocol.command.Command;
import cn.qu.dockman.protocol.command.client.PingCommand;

/**
 * Created by zh on 17/3/16.
 */
public class PingCommandParser extends CommandParser {

    private DispartMgr dispartMgr;

    @Override
    public void before() {
        this.dispartMgr = getApplicationContext().getBean(DispartMgr.class);
    }

    @Override
    public void parse(Command command) {
        PingCommand cmd = (PingCommand) command;
        ArgDef def = new ArgDef(cmd.getHeader().getName(), this.getHost(), this.getPort(),this.getHostname());

        dispartMgr.notifyObservers(def);

    }
}
