package cn.qu.dockman.mgr.controller;

import cn.qu.dockman.mgr.entity.Host;
import cn.qu.dockman.mgr.service.ImageService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by zh on 17/3/7.
 */
@RestController
@RequestMapping("/image")
public class ImageController {

    @Autowired
    private ImageService imageService;

    @RequestMapping(value = "/{hostId}", method = RequestMethod.GET)
    public PageInfo<Host> findAll(@PathVariable int hostId,
                                  @RequestParam(defaultValue = "1") int pageNo,
                                  @RequestParam(defaultValue = "10") int pageSize) {
        return PageHelper.startPage(pageNo, pageSize).doSelectPageInfo(() -> imageService.findByHostId(hostId));
    }

//    @RequestMapping(value = "/info/{id}", method = RequestMethod.GET)
//    public Host findHost(@PathVariable int id) {
//        return imageService.findById(id);
//    }

}
