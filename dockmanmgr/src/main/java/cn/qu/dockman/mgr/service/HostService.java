package cn.qu.dockman.mgr.service;

import cn.qu.dockman.mgr.entity.Host;

import java.util.List;

/**
 * Created by zh on 17/3/7.
 */
public interface HostService {
    List<Host> findByEnvId(int envId);

    Host findById(int id);

    Host findByIp(String ip);

    void updateHost(Host host);

    void insertHost(Host host);
}
