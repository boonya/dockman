package cn.qu.dockman.mgr.service.impl;

import cn.qu.dockman.mgr.entity.Host;
import cn.qu.dockman.mgr.example.HostExample;
import cn.qu.dockman.mgr.mapper.HostMapper;
import cn.qu.dockman.mgr.service.HostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by zh on 17/3/7.
 */
@Service
public class HostServiceImpl implements HostService {

    @Autowired
    private HostMapper hostMapper;

    @Override
    public List<Host> findByEnvId(int envId) {
        List<Host> hostList = hostMapper.selectByExample(new HostExample() {{
            createCriteria().andEnvIdEqualTo(envId);
        }});
        return hostList;
    }

    @Override
    public Host findById(int id) {
        return hostMapper.selectByPrimaryKey(id);
    }

    @Override
    public Host findByIp(String ip) {
        List<Host> hostList = hostMapper.selectByExample(new HostExample() {{
            createCriteria().andHostIpEqualTo(ip);
        }});
        if (!hostList.isEmpty()) {
            return hostList.get(0);
        } else {
            return null;
        }
    }

    @Override
    public void updateHost(Host host) {
        hostMapper.updateByPrimaryKey(host);
    }

    @Override
    public void insertHost(Host host) {
        hostMapper.insert(host);
    }
}
